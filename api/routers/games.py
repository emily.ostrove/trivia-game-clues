from http.client import responses
from operator import truediv
from api.routers import clues
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()

class Games(BaseModel):
    id: int
    espisode_id: int
    aired: str
    canon: bool

class GamesIn(BaseModel):
    title: str

@router.get(
    "/api/games/{game_id}",
    # response_model=Games,
    # responses={404: {"model": Message}},
)
def get_game(id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT id, episode_id, aired, canon
                FROM games
                WHERE id = %s
                """,
                [id],
            )
            row = cur.fetchone()
            print(cur.fetchone())
            record = {
                "id": row[0],
                "episode_id": row[1],
                "aired": row[2],
                "canon": row[3],
            }
            return record

@router.post("/api/custon-games")
def create_game():

# select the data for 30 random clues from the clues
# table where canon is true

# start a transaction

# insert a record into the game_definitions

# for each of the 30 clues, insert the id value
# for each one with the new id from the last step
# into the game_definition_clues

# return the customgame data in the format shown