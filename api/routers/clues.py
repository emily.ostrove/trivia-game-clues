from http.client import responses
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
from routers.categories import CategoryOut
import psycopg

router = APIRouter()

class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool

class Message(BaseModel):
    message: str

class Clues(BaseModel):
    # value: int
    # page_count: int
    # category: CategoryOut
    clues: list[ClueOut]

@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int): #, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id
                    , clues.answer
                    , clues.question
                    , clues.value
                    , clues.invalid_count
                    , categories.id
                    , categories.title
                    , categories.canon
                    , clues.canon
                FROM clues
                JOIN categories
                    ON categories.id = clues.category_id
                WHERE clues.id = %s
                """,
                [clue_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clues not found"}
            record = {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category" : {
                    "id": row[5],
                    "title": row[6],
                    "canon": row[7],
                },
                "canon": row[8],
            }
            # for i, column in enumerate(cur.description):
            #     record[column.name] = row[i]
            return record

@router.get(
    "/api/random-clue",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_random_clue(valid: bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if valid == True:
                cur.execute(
                    """
                    SELECT clues.id
                        , clues.answer
                        , clues.question
                        , clues.value
                        , clues.invalid_count
                        , categories.id
                        , categories.title
                        , categories.canon
                        , clues.canon
                    FROM clues
                    JOIN categories
                        ON categories.id = clues.category_id
                    WHERE clues.invalid_count = 0
                    ORDER BY RANDOM() LIMIT 1
                    """,
                )
            else:
                cur.execute(
                    """
                    SELECT clues.id
                        , clues.answer
                        , clues.question
                        , clues.value
                        , clues.invalid_count
                        , categories.id
                        , categories.title
                        , categories.canon
                        , clues.canon
                    FROM clues
                    JOIN categories
                        ON categories.id = clues.category_id
                    ORDER BY RANDOM() LIMIT 1
                    """,
                )
            row = cur.fetchone()

            record = {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category" : {
                    "id": row[5],
                    "title": row[6],
                    "canon": row[7],
                },
                "canon": row[8],
            }

            return record


@router.get("/api/clues") #, response_model=Clues)
def get_lots_of_clues(value: int, page: int = 0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id
                    , clues.answer
                    , clues.question
                    , clues.value
                    , clues.invalid_count
                    , categories.id
                    , categories.title
                    , categories.canon
                    , clues.canon
                FROM clues
                JOIN categories
                    ON categories.id = clues.category_id
                WHERE clues.value = %s
                LIMIT 100 OFFSET %s
                """,
                [value, page * 100],
            )
            results = []
            for row in cur.fetchall():
                record = {
                    "id": row[0],
                    "answer": row[1],
                    "question": row[2],
                    "value": row[3],
                    "invalid_count": row[4],
                    "category" : {
                        "id": row[5],
                        "title": row[6],
                        "canon": row[7],
                    },
                    "canon": row[8],
                }
                results.append(record)
            
            return Clues(
                # value=value, 
                # page_count=page_count, 
                clues=results)

@router.delete(
    "/api/clues/{clue_id}",
    # response_model=Message,
    # responses={400: {"model": Message}},
)
def update_clue(clue_id: int): ##, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s
                """,
                [clue_id],
            )
    return get_clue(clue_id)